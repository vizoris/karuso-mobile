﻿jQuery(document).ready(function() {
	// Гамбургег-меню
    $(".burger-menu").click(function() {
        $(this).toggleClass("active");
        $(".main-menu").fadeToggle();
    });


// Показать дополнительные параметры доставки
$('.delivery-show__btn').click(function() {
    $(".delivery-show__more").fadeToggle();
});

// Показать всю статью
$('.read-more').click(function() {
    $(this).next(".read-more__show").fadeToggle();
    $(this).fadeOut(100);
});

// Begin of validate 
jQuery.validator.addMethod("checkMask", function(value, element) {
     return /\+\d{1}\(\d{3}\)\d{3}-\d{4}/g.test(value); 
});


$('.delivery-form').validate({
    rules: {
        name: {
            required: true,
        },
        phone: {
            required: true,
            checkMask: true,
        },
    },
    errorPlacement: function() {
        return false;
    }
});

// Маска телефона
$('.phone-mask').mask("+7(999)999-9999");

$(".delivery-form__item input").blur(function(){
    if ($(this).hasClass('valid')) {
        $(this).parent().toggleClass('valid-input');
    }
    if ($(this).hasClass('error')) {
        $(this).parent().removeClass('valid-input');
    }
});

});